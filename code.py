from functools import reduce

rawdata = """This contains the combined data for all languages #_PYTHONfrom functools import reduce

rawdata = #_PYTHON
separator = reduce(lambda c1, c2: c1 + chr(c2), [chr(35), 95, 67, 76, 79, 74, 85, 82, 69])
quote = chr(34)
data = rawdata.split(sep=separator)

print(data[1] + quote + rawdata + quote + data[2])#_PYTHON#_CLOJURE(require '[str :as string]) (do(let [r #_CLOJURE q (char 34)](println (str (second (str/split r (re-pattern (str/join [(char 35) (char 95) (char 82)])))) q r q (nth (str/split r (re-pattern (str/join [(char 35) (char 95) (char 82)]))), 2)))))#_CLOJURE#_Rrawdata <- #_R

data <- strsplit(rawdata, intToUtf8(c(35, 95, 80, 89, 84, 72, 79, 78)))
quote <- intToUtf8(34)
tripleQuote <- paste0(quote, quote, quote)
cat(sprintf(intToUtf8(c(37, 115, 37, 115, 37, 115, 37, 115, 37, 115)), unlist(data)[2], tripleQuote, rawdata, tripleQuote, unlist(data)[3]))#_R"""
separator = reduce(lambda c1, c2: c1 + chr(c2), [chr(35), 95, 67, 76, 79, 74, 85, 82, 69])
quote = chr(34)
data = rawdata.split(sep=separator)

print(data[1] + quote + rawdata + quote + data[2])